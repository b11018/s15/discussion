// alert("Hello Again!");

// Syntax, Statements and Comments
/*
	Statements - programming instructions that we tell our computer/machine to perform
		>> JS statements - usually ends with semi-colon (;)
			>> semi-colon helps to locate where statements end

	Syntax - set of rules describing how statements must be constructed

	Comments - are parts of the code that is ignored
		>> meant to describe the written code
		>> single line comments - double slash
		>> multi-line comments - slash, double asterisks, slash
	>>

*/
// console.log = prints in the console
console.log("Hello World");

// Variables

	// Declaring variables
	/*
		Syntax: let/ const variableName
	*/
	let myVariable;
	console.log(myVariable); //result: undefined

	let hello;
	console.log(hello); //result: not defined
	// let hello; cannot access before initialization

	// Guides:

// Declaring and initializing variables
	/*
		Syntax: let/ const variableName = value;
	*/

// let variables - you can change the value of the variable
let productName = "desktop computer"
console.log(productName);

let price = 18999;
console.log(price);

// const variables - information / values that shouldn't be changed

const interest = 3.539;
console.log(interest);

// Reassigning variables
/*
	Syntax: variableName = value;
*/
/*camel casing - lowercase letter then upercase letter*/
productName = "laptop";
console.log(productName);

let friend = "Min";
friend = "Jane";
console.log(friend); // result: Jane

//let friend = "Kate"
console.log(friend); //result: error: friend has already been declared

//interest = 3.614;
//console.log(interest); // result: assignment to constant variable

let role = "Supervisor";
let name = "Edward";

/*
	Mini-Activity:
	1. print out the value of role and name
	2. re-assign the value of role to director
	3. send a screenshot of your console with the output
*/

console.log(role);
console.log(name);
role = "Director";
console.log(role);

// Reassigning variables vs. Initializing variables

// Declare a variable
let supplier;
// initialization - done after a variable has been declared
supplier = "Jane Smith Tradings";
console.log(supplier); // result: Jane Smith Tradings

// reassigning - done after giving initial value to the variable
supplier = "Zuitt Store";
console.log(supplier); // result: Zuitt Store

// var vs. let/ const

// var - was used from 1997 to 2015
// let/ const - ES6 updates (2015)
// const variable should be declared and initialized the same time

// Hoisting of var - JavaScript default behavior of moving declaration to the top
a =  5;
console.log(a); //result: 5
var a;

// let / const local and global scope
// Scope - essentially means where these variables are available for use

let outerVariable = "Hello";

{	
	// local variable
	// let / const are block scoped
	// block is a chunk of code surrounds by {}. A block lives in curly braces
	let innerVariable = "Hello Again";
};

//console.log(outerVariable); //result: Hello
//console.log(innerVariable); //result: error

// Multiple Variable Declarations

let productCode = "DC017", productBrand = "Dell";
// let productCode = "DC017";
// const productBrand = "Dell";
console.log(productCode, productBrand);
// result: DC017, Dell

// const let = "hello";
// console.log(let); //result: error - let is a reserved keyword

// Data Types

/*Strings
	- series of characters that create a word, phrase, a sentence or anything related to text
	- strings are wrapped around quotation marks ('') or ("")
*/

let country = "Philippines";
let city = 'Caloocan City';
let numberString = "123456"

// Concatenating Strings
let fullAddress = city + ',' + country;
console.log(fullAddress); //result: Caloocan City, Philippines

//solution: (miniAct2)
let myName = 'JP';
let greeting = 'Hi I am ' + myName
console.log(greeting)

//escape character (\)
let mailAddress = 'Metro manila\n\nPhilippines';
console.log(mailAddress); // result: line break between MM and PH

let message = "John's father is coming home today";
//prints out the message
console.log(message);
message = 'John\'s father is coming home tomorrow';
console.log(message);
//prints out the message using escape character

let pet = "pillow";
console.log(pet); // result: pillow

// Number
// Integers/Whole Numbers
let count = 64;
console.log(count); // result: 64

// Decimal Number/Fractions
let grade = 98.7;
console.log(grade); //result: 98.7

//Exponential Notations
let planetDistance = 2e10;
console.log(planetDistance); //result: 20000000000

// Concatenate text and numbers
console.log('Jino\'s grade last quarter is' + ' ' + grade);
// result: Jino's grade last quarter is 98.7

// consider where will you use it and if it will be used in mathematical operations
let numberString1 = "09912345678";
let number  = 4578;

/* Boolean
	- the values relate to a state of a certain thing
	- this will be useful in future discussion considering logic
*/

let isSingle = true;
let inGoodConduct = false;
console.log("isSingle" + ' ' + isSingle);
console.log("inGoodConduct" + ' ' + inGoodConduct);

// additional info:
	// 1 equivalent to true
	// 0 equivalent to false

/* Arrays
	-special kind of data type

	let / const arrayName = [elementA, elementB ...]
*/

let anime = ["Naruto", "Bleach", "Attack On Titan", "Demon Slayer", "Spy x Family"];
console.log(anime);

let quarterlyGrades = [95, 96.3, 87, 90];
console.log(quarterlyGrades);

//it works but doesn't make sense in the context of programming and not recommended
let random = ["JK", 24, true];
console.log(random);

/* Objects
	- special kind of data type
	
	Syntax:
	let / const objectName = {
	propertyA: valueA,
	propertyB: valueB
	}
	key-value pairs
*/
let person = {
	fullName: "Midoriya Izuku",
	age: 15,
	isStudent: true,
	contactNo: ["09123456789", "8123 4567"],
	address: {
		houseNumber: "568",
		city: "Tokyo"
	}
};
console.log(person);

// Null
let spouse = null;
console.log(spouse);

let emptyString = "";
console.log(emptyString);

let myNumber = 0;
console.log(myNumber);